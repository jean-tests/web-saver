# ᛠᛖᛒ ᛋᚨᚡᛖᚱ
ᛈᛖᚱᛗᛖᛏ ᛞᛖ ᛋᚨᚡᛖᚸᚨᚱᛞᛖᚱ ᛞᛖᛋ ᚠᛚᚢᛪ ᛏᛖᛪᛏᛖ ᛈᚨᚱ ᛇᚾᛏᛖᚱᚾᛖᛏ

## ᚲᛟᚾᚠᛇᚸᚢᚱᚨᛏᛇᛟᚾ
```shell script
$ docker run -v websaver-data:/datastore -p 8080:8080 jeanribes/web-saver /main --file /datastore/content.txt
```

## ᚢᛏᛇᛚᛇᛋᚨᛏᛇᛟᚾ
```shell script
# ᛖᚾᚡᛟᛡᛖᚱ ᛞᛖᛋ ᛞᛟᚾᚾᛖᛖᛋ
$ curl -X POST -d "ᛞᛟᚾᚾᛖᛖᛋ ᛏᛖᛪᛏᛖ" https://ᚺᛟᛋᛏᚾᚨᛗᛖ:8080/write

# ᚱᛖᚲᛖᚡᛟᛇᚱ ᛞᛖᛋ ᛞᛟᚾᚾᛖᛖᛋ
$ curl https://ᚺᛟᛋᛏᚾᚨᛗᛖ:8080/write
```