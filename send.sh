#!/usr/bin/env bash
if [ -t 0 ]
then
    data=$1
else
    data=$(cat)
fi

echo "$data"
#curl -X POST -d "$data" https://fd.ribes.ovh/write >> /dev/null 2>&1
curl -X POST -d "$data" http://localhost:8080/write >> /dev/null 2>&1
#curl http://localhost:8080/read