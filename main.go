package main

import (
	"flag"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	var portP = flag.String("port", "8080", "Port sur lequel écouter en HTTP")
	var fileP = flag.String("file", "data.txt", "Fichier où enregistrer les données")
	flag.Parse()
	fmt.Println("Démarrage sur le port " + *portP)
	fmt.Println("Utilisation du ficher : " + *fileP)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println(err)
		}
		fmt.Println(string(body))
		ancienFichier, _ := ioutil.ReadFile(*fileP)
		_, _ = fmt.Fprintf(w, string(ancienFichier))
	})
	http.HandleFunc("/write", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.OpenFile(*fileP,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println(err)
		}
		defer f.Close()
		bbody, _ := ioutil.ReadAll(r.Body)
		body := string(bbody)
		if _, err := f.WriteString(body + "\n"); err != nil {
			log.Println(err)
		}
		_, _ = fmt.Fprintf(w, "Utilisez une méthode POST\n")

	})
	http.HandleFunc("/read", func(w http.ResponseWriter, r *http.Request) {
		ancienFichier, _ := ioutil.ReadFile(*fileP)
		fmt.Fprintf(w, string(ancienFichier))
	})

	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(*portP)
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})
	log.Fatal(http.ListenAndServe(":"+*portP, nil))
}
